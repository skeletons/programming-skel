Implementation of the languages and the examples presented in the paper "A Practical Approach for Describing Language Semantics", submitted to "The Art, Science, and Engineering of Programming" journal.

# Installation
## Dependencies (installed in the next step)

- necrolib
- necroml

## How to install dependencies locally

```
git clone
opam switch create . 4.14.1
eval $(opam env)
opam repository add necro https://gitlab.inria.fr/skeletons/opam-repository.git#necro
opam pin add necrolib 0.11.3
opam install necroml
```


# How to run

```
cd modular
dune exec arith
dune exec arith_div
dune exec pcf
dune exec state
dune exec yield
```
