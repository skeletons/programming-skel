open Yield

module Inst(M:MONAD) = struct

  module rec Types : sig
    include Arith_inst.TYPE
    include Env_inst.TYPE with type value = Unspec(M)(Types).value
    include Heap_inst.TYPE with type value = Unspec(M)(Types).value
  end = Types

  module Spec = struct
    include Unspec(M)(Types)
    include Arith_inst.Spec(M)
    include Arith_div_inst.Spec(M)
    include Env_inst.Spec(M)
    include Heap_inst.Spec(M)
  end

  module Interp = MakeInterpreter(Spec)
end

module InterpInst = Inst(Necromonads.ContPoly)

open InterpInst.Interp

let fact1 v =  LetIn ("v",
                     (Ref v),
                     LetIn ("fact",
                            Fix ("factrec",
                                 "r",
                                 Ifz(Get (Var "r"),
                                     Const 1,
                                     (Seq (Set (Var "r",Sub(Get(Var "r"), Const (1))),
                                           Mul (Add(Get(Var "r"),Const 1),
                                                App(Var "factrec", Var "v"))
                                        )
                                     )
                              )),
                            App (Var "fact", Var "v")
                ))


let fact =  Fix ("factrec", "x",
                 Ifz(Var "x",
                     Const 1,
                     Mul (Var "x",App(Var "factrec", Sub (Var "x", Const 1)))
                   )
              )

let run t  = M.extract (M.extract (M.extract (init t) initialEnv) initialHeap)

let run_ok t d = match (run t) with
  | Ok (Nat n), _ -> Printf.printf "%s --> %d\n" d n
  | _ -> assert false

let _ = run_ok (fact1 (Const 5)) "5!"

let figure8 =
        LetIn ("x",
               Run (Seq (Yield (Const 1),Seq (Yield (Const 2),Const 3))),
               Handle (Var "x",
                       ("_", Skip),
                       ("v1", "k1", Handle (Resume (Var "k1",Skip),
                                          ("v2", Var "v2"),
                                          ("v2", "k2",
                                           Handle (Resume (Var "k2", Skip),
                                                   ("v3", Add (Mul (Var "v1", Const 100),
                                                               Add (Mul (Var "v2", Const 10),
                                                                    Var "v3"))),
                                                       ("_","_", Skip)))))))

let _ = run_ok figure8 "\nlet x = {yield 1; yield 2; 3} in\nhandle(x,_,\\v1,k1.\n  handle(resume k1,_,\\v2,k2.\n    handle(resume k2, \\v3. 100 * v1 + 10 * v2 + v3,_)))"

let figure8_1 =
        LetIn ("x",
               Run (Seq (Yield (Const 1),Seq (Yield (Const 2),Const 3))),
               Handle (Var "x",
                       ("_",Skip),
                       ("v1", "k1",
                        Handle (Resume (Var "k1",Skip),
                                ("_", Skip),
                                ("v2", "k2",
                                 Handle (Resume (Var "k2", Skip),
                                         ("v3", App(fact,Var "v3")),
                                         ("_","_",Skip)))))))

let _ = run_ok figure8_1 ""

let figure8_2 =
        LetIn ("x",
               Run (Seq (Yield (Const 1),Seq (Yield (Const 2), Const 3))),
               Handle (Var "x",
                       ("_",Skip),
                       ("v1","k1",
                        Handle (Resume (Var "k1",Skip),
                                ("_", Skip),
                                ("v2","_",App(fact,Var "v2"))))))

let _ = run_ok figure8_2 ""

let figure_9 =
          LetIn ("x", Run (LetIn ("y",
                                  (Const 1),
                                  LetIn ("z", Yield (Var "y"), Add(Mul (Var "y", Const 100),
                                                                   Add(Mul (Var "z", Const 10),
                                                                       Yield (Const 0)))))),
                 Handle (Var "x",
                         ("_", Skip),
                         ("v1", "k1",
                          Handle (Resume(Var "k1", Add (Var "v1", Const 1)),
                                  ("_", Skip),
                                  ("v2","k2",
                                   Handle (Resume (Var "k2", Const 3),
                                           ("v3", Var "v3"),
                                           ("_", "_", Skip)
                                           ))))))

let _ = run_ok figure_9 ""

(*communicating values back. each time we handle a suspension, we multiply the yield value by it's factorial result*)

let sum_eval = Run (Add(Const 1,Const 2))

let _ = run_ok sum_eval ""

let sum_yield_eval = Run (Add (Yield (Const 1),Const 2))

let _ = match run sum_yield_eval with
  | Ok (Susp (Nat v, _)), _ -> Printf.printf "\n{yield 1 + 2} --> %d (suspended)\n" v
  | _ -> assert false

let handle_sum_eval = Handle (Add(Run (Add(Const 1,Const 2)),Const 3),
                              ("v1", Var "v1"),
                              ("v1","_",Var "v1"))


let _ = run_ok handle_sum_eval ""

let handle_sum_yield_eval = Handle (Run (Add (Yield (Const 1),Const 2)),
                              ("v1", Var "v1"),
                              ("v1","k1",
                               Handle ((Resume (Var "k1",Add (Var "v1",Const 3))),
                                       ("v2", Var "v2"),
                                       ("v2", "_",Var "v2")
                                       )))

let _ = run_ok handle_sum_yield_eval ""

let figure_13 =
          LetIn ("x", Run (LetIn ("y", Ref (Const 1),
                                  LetIn ("x1", Get (Var "y"),
                                         LetIn ("x2", Get (Yield (Var "y")),
                                                Seq (Yield (Set (Var "y",Const 1)),
                                                     Add (Mul (Const 100, Var "x1"),
                                                          Add (Mul (Const 10, Var "x2"),
                                                               Get (Var "y")))))))),
                 Handle (Var "x",
                         ("v1", Var "v1"),
                         ("ref","k1",
                          Seq (Set (Var "ref", Const 2),
                               Handle (Resume(Var "k1", Var "ref"),
                                       ("v2", Var "v2"),
                                       ("_", "k2",
                                        Seq (Set (Var "ref", Add(Get (Var "ref"),Const 2)),
                                             Handle (Resume (Var "k2", Var "_"),
                                                     ("v3", Var "v3"),
                                                     ("v3", "_", Var "v3")))))))))
let _ = run_ok figure_13 ""
