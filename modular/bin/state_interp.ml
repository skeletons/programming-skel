open State

module Inst(M:MONAD) = struct

  module rec Types : sig
    include Arith_inst.TYPE
    include Env_inst.TYPE with type value = Unspec(M)(Types).value
    include Heap_inst.TYPE with type value = Unspec(M)(Types).value
  end = Types

  module Spec = struct
    include Unspec(M)(Types)
    include Arith_inst.Spec(M)
    include Arith_div_inst.Spec(M)
    include Env_inst.Spec(M)
    include Heap_inst.Spec(M)
  end

  module Interp = MakeInterpreter(Spec)
end

module InterpInst = Inst(Necromonads.ContPoly)

open InterpInst.Interp

let fact v =  LetIn ("v",
                     (Ref v),
                     LetIn ("fact",
                            Fix ("factrec",
                                 "r",
                                 Ifz(Get (Var "r"),
                                     Const 1,
                                     (Seq (Set (Var "r",Sub(Get(Var "r"), Const (1))),
                                           Mul (Add(Get(Var "r"),Const 1),
                                                App(Var "factrec", Var "v"))
                                        )
                                     )
                              )),
                            App (Var "fact", Var "v")
                ))



let run t  = M.extract (M.extract (M.extract (eval t) initialEnv) initialHeap)

let _ = match run (fact (Const 5)) with
  | Ok (Nat n), _ -> Printf.printf "5! --> %d\n" n
  | _ -> assert false
