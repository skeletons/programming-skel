open Arith_div

module Inst(M:MONAD) = struct
  module rec Types : sig
    include Arith_inst.TYPE
  end = Types

  module Spec = struct
    include Unspec(M)(Types)
    include Arith_inst.Spec(M)
    include Arith_div_inst.Spec(M)
  end

  module Interp = MakeInterpreter(Spec)
end

module InterpInst = Inst(Necromonads.ContPoly)

open InterpInst.Interp

let _ = match M.extract (eval (Add (Const 1,Const 2))) with
  | Ok (Nat n) -> Printf.printf "1+2 --> %d\n" n
  | _ -> assert false
let _ = match M.extract (eval (Sub (Const 1,Const 2))) with
  | Ok (Nat n) -> Printf.printf "1-2 --> %d\n" n
  | _ -> assert false
let _ = match M.extract (eval (Div (Const 2,Const 2))) with
  | Ok (Nat n) -> Printf.printf "2/2 --> %d\n" n
  | _ -> assert false
let _ = match M.extract (eval (Div (Const 2,Const 0))) with
  | Exc -> print_endline "2/0 --> Exception"
  | _ -> assert false
let _ = match M.extract (eval (Mul (Const 2,Const 0))) with
  | Ok (Nat n) -> Printf.printf "2*0 --> %d\n" n
  | _ -> assert false
let _ = match M.extract (eval (Mul (Const 0,Const 0))) with
  | Ok (Nat n) -> Printf.printf "0*0 --> %d\n" n
  | _ -> assert false
let _ = match M.extract (eval (Try (Div (Const 2,Const 0),
                              Div (Const 84,Const 2)))) with
  | Ok (Nat n) -> Printf.printf "try 2/0 with 84/2 --> %d\n" n
  | _ -> assert false
