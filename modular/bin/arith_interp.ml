open Arith

module Inst(M:MONAD) = struct
  module rec Types : sig
    include Arith_inst.TYPE
  end = Types

  module Spec = struct
    include Unspec(M)(Types)
    include Arith_inst.Spec(M)
  end

  module Interp = MakeInterpreter(Spec)
end

module InterpInst = Inst(Necromonads.ContPoly)
open InterpInst.Interp

let run t d = match M.extract (eval t) with Nat n -> Printf.printf "%s --> %d\n" d n

let _ = run (Add (Const 1,Const 2)) "1+2"
let _ = run (Sub (Const 1,Const 2)) "1-2"
