open Necromonads

module SMap = Map.Make(String)

module type TYPE = sig
  type value
  type var = string
  type env = value SMap.t
end

module Spec(M:MONAD) = struct
  let initialEnv = SMap.empty
  let extEnv s = M.ret (fun x -> M.ret (fun v -> M.ret (SMap.add x v s)))
  let getEnv s = M.ret (fun x -> (match SMap.find_opt x s with
      | Some v -> M.ret v
      | None -> M.fail "the variable is not bound in the environment" ))
end
