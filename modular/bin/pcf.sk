type nat
type var
type env

type value = | Nat nat | Clos (var,expr,env) | ClosRec (var,var,expr,env) | Unit

type expr =
| Const nat
| Add (expr,expr)
| Sub (expr,expr)
| Div (expr,expr)
| Mul (expr,expr)
| Try (expr,expr)
| Var var
| Lam (var,expr)
| App (expr,expr)
| LetIn (var,expr,expr)
| Ifz (expr,expr,expr)
| Fix (var,var,expr)
| Seq (expr,expr)
| Skip

type exc<a> = | Exc | Ok a

type rd<a> := env -> a
type m<a> := rd<exc<a>>

val return<a> (v:a) : m<a> = λ_:env → Ok<a> v
val throw<a> (():()) : m<a> = λ_:env → Exc<a>
val bind<a,b> (v:m<a>) (f:a → m<b>) : m<b> =
    λs:env →
        let vo = v s in
        match vo with
        | Exc → throw<b> () s
        | Ok v' → f v' s
        end
val exccont<a> (v:m<a>) (f:() → m<a>) : m<a> =
    λs:env →
        let vo = v s in
        match vo with
        | Exc → f () s
        | Ok v → return<a> v s
        end

val ask : m<env> = λs:env → Ok<env> s

val local<a> (w:m<env>) (f: () → m<a>) : m<a> =
  λs:env → let Ok s' = w s in  f () s'

binder ? := exccont
binder @ := bind

type output := m<value>

val retenv (s:env) : m<env> = return<env> s
val ret (v:value) : m<value> = return<value> v
val thr (():()) : m<value> = throw<value>()

val add : (nat,nat) -> nat
val sub : (nat,nat) -> nat
val div : (nat,nat) -> nat
val mul : (nat,nat) -> nat

val zero : nat -> ()
val not_zero : nat -> ()

val getEnv : env -> var -> value
val extEnv : env -> var -> value -> env
val initialEnv : env

val envGet (x:var) : output =
    let s =@ ask in
    let v = getEnv s x in ret v

val envExt ((x,v):(var,value)) : m<env> =
    let s =@ ask in
    let s' = extEnv s x v in
    retenv s'

val eval (e:expr) : output =
    match e with
    | Const n → ret (Nat n)
    | Add (e1,e2) →
           let Nat n1 =@ eval e1 in
           let Nat n2 =@ eval e2 in
           let n = add(n1,n2) in ret (Nat n)
    | Sub (e1,e2) →
           let Nat n1 =@ eval e1 in
           let Nat n2 =@ eval e2 in
           let n = sub(n1,n2) in ret (Nat n)
    | Div (e1,e2) →
           let Nat n1 =@ eval e1 in
           let Nat n2 =@ eval e2 in
           branch zero n2; thr()
           or     not_zero n2;
                  let n = div(n1,n2) in ret (Nat n)
           end
    | Mul (e1,e2) →
           let Nat n1 =@ eval e1 in
           let Nat n2 =@ eval e2 in
           let n = mul(n1,n2) in ret (Nat n)
    | Try (e1,e2) →
           eval e1;? eval e2
    | Var x →
           envGet x
    | Lam (x,eb) →
           let s =@ ask in
           ret (Clos (x, eb, s))
    | Fix (f, x, ex) →
           let s =@ ask in
           ret (ClosRec (f, x, ex, s))
    | App (e1,e2) →
           let cl =@ eval e1 in
           let v =@ eval e2 in
           match cl with
           | Clos (x, eb, s) →
                  retenv s;%local
                  envExt(x, v);%local
                  eval eb
           | ClosRec (f, x, eb, s) →
                  retenv s;%local
                  envExt(f, cl);%local
                  envExt(x, v);%local
                  eval eb
           | _ → thr()
           end
    | LetIn (x,ex,ec) →
           let v =@ eval ex in
           envExt(x, v);%local
           eval ec
    | Ifz (ec,e1,e2) →
           let Nat n =@ eval ec in
           branch zero n; eval e1
           or     not_zero n; eval e2
           end
    | Seq (e1,e2) →
           eval e1;@
           eval e2
    | Skip →
           ret Unit
    end
